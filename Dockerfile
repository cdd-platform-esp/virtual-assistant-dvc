FROM rasa/rasa:2.7.0
USER root
COPY  ./requirements.txt /app/requirements.txt
WORKDIR /app
RUN apt-get update &&  apt-get install -y git
RUN pip3 install --upgrade pip 
RUN pip3 install -r requirements.txt
USER 1001
ENTRYPOINT [""] 
